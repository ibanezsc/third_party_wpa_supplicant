/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 */


#ifndef VENDOR_EXT_H
#define VENDOR_EXT_H

int vendor_ext_test1();

#endif // VENDOR_EXT_H
